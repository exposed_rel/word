   105.dent= tooth表示"牙齿"
   dentist		[ˈdɛntɪst]n.牙科医生
   denture		[ˈdɛntʃə]n.假牙
   indenture	[ɪnˈdɛntʃə]n.契约,合同
   edentate		[ˈiːdənteɪt]a.无齿的n.贫齿类动物
   dent			[dɛnt]n.凹部,凹痕;缺口v在(某物)上造成凹痕
   
   106. limin,lim= threshold表示"门槛",引申为"限制"
   limit		[ˈlɪmɪt]v./n.界限,限制;极点
   eliminate	[ɪˈlɪmɪneɪt]v.消除
   preliminary	[prɪˈlɪmɪn(ə)ri]a.预备的,初步的n.[常p]初步作法,起始行为
   limp			[lɪmp]v.蹒跚n.跛行a.软弱的,无生气的;松沓的
   supraliminal	[ˌsuːprəˈlɪmɪnl]a.有意识的
   
   47. proach, proxim=to close, to become near表示"接近,靠近"
★  approach			v接近,向……靠近;n.近路;方法;靠近		 /ə'proʊtʃ/
   approachable		a易接近的,平易近人的		 		 /ə'proʊtʃəbl/ 
   approximate		v近似;估计;a大约的,大概,近似的		 /ə'prɑksɪmət/ 
   approximation	n近似,近似值	 					 /əˌprɑːksɪ'meɪʃn/ 
   proximity		n接近	 							 /prɑːk'sɪməti/ 
★  reproach			v责备,批评,斥骂                      /rɪˈproʊtʃ/
   
   48.init= beginning表示"开始"
★  initial			a最初的,初始;n.首字母大写	 /ɪ'nɪʃl/ 
★  initiate			v发起,开始,开创;使人初步了解;接纳(新成员);n新加入者	 /ɪ'nɪʃieɪt/ 
★  initiation		n启蒙,传授；开始,引发;入会 	 /ɪˌnɪʃi'eɪʃn/ 
   initialize		v初始化						 /ɪ'nɪʃəlaɪz/ 
★  initially		adv最初,开头,首先,原来		 /ɪ'nɪʃəli/  
   
   77.pass=togo, to pass through表示"走;通过"
★  passage			n通道,通过,(文章)片段,一节  ˈpasɪdʒ
   passport			n护照                       ˈpɑːspɔːt
★  compass			n罗盘,指南针                ˈkʌmpəs
★  surpass			v超越,胜过                  səˈpɑːs
★  trespass			v侵入,打扰,冒犯,犯罪        ˈtrɛspəs
   
   78. habit, hibit= to have, to hold表示"有;拿"
★  habit			n习惯                  ˈhabɪt
   habitual			a惯常的                həˈbɪtʃʊəl
   habitat			n(动植物)栖息地       ˈhabɪtat
★  habitant			n居住者                ˈhabɪt(ə)nt
   inhabit			v居住                  ɪnˈhabɪt
★  inhabitant		n住户,居民             ɪnˈhabɪt(ə)nt
★  prohibit			v阻止,禁止             prə(ʊ)ˈhɪbɪt
★  exhibit			v展览,展示,显示;n展览  ɪɡˈzɪbɪt

   91.simil, simul(t),sembl= alike,same表示“相类似,一样
   similar			a同样的;相似的,像                ˈsɪmɪlə
   similarity		n类似,相似点                      sɪməˈlarəti
   dissimilar		a不同的                          dɪˈsɪmɪlə
   verisimilar		a像真的一样,貌似真实的;可能的    ˌvɛrɪˈsɪmɪlə
   simulate			v假装,模仿,模拟;冒充             ˈsɪmjʊleɪt ,ate使一样->模仿
   simultaneous		a同时发生的,同时存在的,同步的    ˌsɪm(ə)lˈteɪnɪəs ,aneous...特征->相识的特征
   resemble			v类似,像                         rɪˈzɛmb(ə)l
   resemblance		n类似,相似,形似                  rɪˈzɛm bl(ə)ns
   assemble			v集合,召集,装配                  əˈsɛmb(ə)l
   
   92.vi(a),vey,voy=way表示“道路”
   via				[介词prep]经过      ˈvʌɪə
   viaduct			n高架桥             ˈvʌɪədʌkt
   deviate			v越轨,背离          ˈdiːvɪeɪt
   obvious			a明显的             ˈɒbvɪəs
   previous			a以前的,早先的      ˈpriːvɪəs
   trivial			a琐碎的;不重要的    ˈtrɪvɪəl
   convey			v运输,传达;搬运     kənˈveɪ
   convoy			v护送,护航          ˈkɒnvɔɪ

   99.tour,torn, tourn= to turn表示转"
   tour			[tʊə]v/n.旅游,参观,观光
   tournament	[ˈtʊənəm(ə)nt]n.锦标赛,比赛
   contour		[ˈkɒntʊə]n.轮廓
   detour		[ˈdiːtʊə]v.绕路n.便道;绕道
   attorney		[əˈtəːni]n.律师,(业务或法律事务的)代理人
   
   100. quiet,qui= still,rest表示"静,静止"
   quiet		[ˈkwʌɪət]a.宁静的,安静的
   disquiet		[dɪsˈkwʌɪət]v.使不安;使担心n.不安;忧虑
   quietude		[ˈkwʌɪɪtjuːd]n.安静
   inquietude	[ɪnˈkwʌɪətjuːd]n.不安
   acquiescence	[ˌakwɪˈɛsns]n.默许
   
   103. gen(e), gener, geni(t)= 表示"出生,产生;基因,种族"
   gene			[dʒiːn]n.基因
   generation	[dʒɛnəˈreɪʃ(ə)n]n.产生;一代
   engender		[ɪnˈdʒɛndə]vt.使产生；造成
   generate		[ˈdʒɛnəreɪt]v.产生,引起;发电;生殖
   genius		[ˈdʒiːnɪəs]n.天才,天赋
   ingenious	[ɪnˈdʒiːnɪəs]a.聪明的;有创造力的;(方法等)别致的;灵巧的
   genuine		[ˈdʒɛnjʊɪn]a.真正的,纯正的;真诚的
   generous		[ˈdʒɛn(ə)rəs]a.慷慨的;丰富的
   generosity	[dʒɛnəˈrɒsəti]n.慷慨
   
   104.cap(it),cip(it)=head表示"头"
   capital		[ˈkapɪt(ə)l]a.首要的;n.首都,省会;资本
   cape			[keɪp]n.海角,岬;斗篷,披肩
   precipitous	[prɪˈsɪpɪtəs]a.陡峭的;急躁的,仓促的
   capitulate	[kəˈpɪtjʊleɪt]v.(有条件)投降;屈从,停止抵抗
   precipitate	[prɪˈsɪpɪteɪt]v.(突如其来地)发生,促成;降水.沉淀物a.突如其来的,鲁莽的
   
   105.dent= tooth表示"牙齿"
   dentist		[ˈdɛntɪst]n.牙科医生
   denture		[ˈdɛntʃə]n.假牙
   indenture	[ɪnˈdɛntʃə]n.契约,合同
   edentate		[ˈiːdənteɪt]a.无齿的; n.贫齿类动物
   dent			[dɛnt]n.凹部,凹痕;缺口v在(某物)上造成凹痕
   
   106. limin,lim= threshold 表示"门槛",引申为"限制"
   limit		[ˈlɪmɪt]v./n.界限,限制;极点
   eliminate	[ɪˈlɪmɪneɪt]v.消除
   preliminary	[prɪˈlɪmɪn(ə)ri]a.预备的,初步的n.[常p]初步作法,起始行为
   limp			[lɪmp]v.蹒跚n.跛行a.软弱的,无生气的;松沓的
   supraliminal	[ˌsuːprəˈlɪmɪnl]a.有意识的
   
