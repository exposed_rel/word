   83. cover= to cover, cover表示"遮盖盖子"
★  cover			v包括,采访,报道,覆盖;n封面            ˈkʌvə
   discover			v发现,发觉                            dɪˈskʌvə
   discovery		n发现,发觉,发现物                     dɪˈskʌv(ə)ri
★  recover			v重新获得,使康复,痊愈                 rɪˈkʌvə
★  coverage			n新闻报道,覆盖范围                    ˈkʌv(ə)rɪdʒ
★  covert			a隐秘的,秘密的,不公开的; n隐蔽处      ˈkʌvət
   hardcover		n精装本;a精装的                       ˈhɑːdkʌvə
   softcover		n软皮,软面                            sɒftˈkʌvə
   
   84. toler= to endure, to bear表示"容忍,忍受"
★  tolerate			v忍受,容忍                           ˈtɒləreɪt
   tolerable		a可容忍的,还不错的                   ˈtɒl(ə)rəb(ə)l
   tolerant			a宽容的,容忍的,心胸宽大的            ˈtɒl(ə)r(ə)nt
★  intolerant		a不能容忍的                          ɪnˈtɒl(ə)r(ə)nt
   intolerance		n不能忍受,不宽容,偏狭                ɪnˈtɒl(ə)r(ə)ns

   113. front= forehead表示"前额,前面"
   front		[frʌnt]n.前面,正面ad.在前面,向前a.前面的;正面的
   frontal		[ˈfrʌnt(ə)l]a.正面的;前额的
   confront		[kənˈfrʌnt]v.面临;对抗;使对质
   affront		[əˈfrʌnt]v.冒犯
   forefront	[ˈfɔːfrʌnt]n.最前部;(思考、关注的)重心;(运动、活动的)前沿
   
   114. touch= to touch表示"触,摸,碰"
   touch		[tʌtʃ]v.摸,接触;感动n.接触;联系
   untouchable	[ʌnˈtʌtʃəb(ə)l]a.不可接触的
   touched		[tʌtʃt]a.被感动的;感激的
   touching		[ˈtʌtʃɪŋ]a.动人的
   touchy		[ˈtʌtʃi]a.易怒的;棘手的;敏感的
   
