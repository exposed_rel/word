   103. gen(e), gener, geni(t)= 表示"出生,产生;基因,种族"
   gene			[dʒiːn]n.基因
   generation	[dʒɛnəˈreɪʃ(ə)n]n.产生;一代
   engender		[ɪnˈdʒɛndə]产生
   generate		[ˈdʒɛnəreɪt]v.产生,引起;发电;生殖
   genius		[ˈdʒiːnɪəs]n.天才,天赋
   ingenious	[ɪnˈdʒiːnɪəs]a.聪明的;有创造力的;(方法等)别致的;灵巧的
   genuine		[ˈdʒɛnjʊɪn]a.真正的,纯正的;真诚的
   generous		[ˈdʒɛn(ə)rəs]a.慷慨的;丰富的
   generosity	[dʒɛnəˈrɒsəti]n.慷慨
   
   104.cap(it),cip(it)=head表示"头"
   capital		[ˈkapɪt(ə)l]a.首要的n.首都,省会;资本
   cape			[keɪp]n.海角,岬;斗篷,披肩
   precipitous	[prɪˈsɪpɪtəs]a.陡峭的;急躁的,仓促的
   capitulate	[kəˈpɪtjʊleɪt]v.(有条件)投降;屈从,停止抵抗
   precipitate	[prɪˈsɪpɪteɪt]v.(突如其来地)发生,促成;降水.沉淀物a.突如其来的,鲁莽的
   
