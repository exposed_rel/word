   65.em,empt,am,ampl= to take, to procure表示"拿;获得"
★  ample			a充足的                         'amp(ə)l
★  redeem			v赎回,买回,偿还                 rɪ'diːm
★  exempt			v免除,豁免; a被免除的,被豁免的  ɪɡ'zɛm(p)t
   exemption		n免除,豁免                      ɪɡ'zɛmpʃn
   example			n例子;榜样                      ɪɡ'zɑːmp(ə)l
★  exemplify		v举例说明,是...的典范           ɪɡ'zɛmplɪfʌɪ

   66.bat= to beat表示"打,击"
   battle			n战役,战斗,斗争; v作战,斗争  'bat(ə)l
★  combat			v/n战斗;搏斗                 'kɒmbat
★  debate			v/n讨论,辩论                 dɪ'beɪt
★  rebate			v减少;打折扣; n回扣,折扣     'riːbeɪt
★  abate			v(风力、声音、痛苦)减小,减轻;减(税),降价    ə'beɪt

   95. gress=to go, to walk表示"行走"
   progress		[ˈprəʊɡrɛs]n.进步
   congress		[ˈkɒŋɡrɛs]n.国会,代表大会
   digress		[dʌɪˈɡrɛs]v离题
   retrogress	[ˌrɛtrə(ʊ)ˈɡrɛs]v.倒退,退步
   transgress	[tranzˈɡrɛs]v.违反;违背;冒犯
   aggression	[əˈɡrɛʃ(ə)n]n.侵略;进攻
   
   96. polic, polit, polis= state,city表示"国家,城市",引申为"政治"
   police		[pəˈliːs]n.警察
   policy		[ˈpɒlɪsi]n.政策,方针;保险单
   politician	[pɒlɪˈtɪʃ(ə)n]n.政客
   politics		[ˈpɒlɪtɪks]n.政治,政治学;政纲,政见
   metropolis	[mɪˈtrɒp(ə)lɪs]n.大都市
   
   109.vail,val(u)= value, worth表示"价值,值"
   available	[əˈveɪləb(ə)l]a.有效的;有空的;可得到的
   valuable		[ˈvaljʊb(ə)l]a.有价值的;贵重的
   evaluate		[ɪˈvaljʊeɪt]v.估价
   equivalent	[ɪˈkwɪv(ə)l(ə)nt]a.相等的n.等价物
   invaluable	[ɪnˈvaljʊ(ə)b(ə)l]a.非常宝贵的,无价的
   undervalue	[ʌndəˈvaljuː]v.低估;轻视
   
   110.jud,judg, judic(i)= to judge,law表示"判断;法律"
   prejudice	[ˈprɛdʒʊdɪs]n.偏见
   adjudge		[əˈdʒʌdʒ]判决,裁决
   judgment		[ˈdʒʌdʒmənt]n.判断,判断力;审判;意见
   prejudge		[priːˈdʒʌdʒ]v.预先断定
   judicial		[dʒuːˈdɪʃ(ə)l]a.司法的;法官的
   adjudicate	[əˈdʒuːdɪkeɪt]v.判决,裁判
   
   117. numer= number, to count表示"数;计数"
   numerable	[ˈnjuːm(ə)rəb(ə)l]a.可数的,可计算的
   numerous		[ˈnjuːm(ə)rəs]a.无数的,众多的
   innumerable	[ɪˈnjuːm(ə)rəb(ə)l]a.无数的,数不清的
   supernumerary[ˌsuːpəˈnjuːm(ə)r(ə)ri]   supernumerary	[sju: panju: maram.额外的
   enumerate	[ɪˈnjuːməreɪt]v.列举
   
   118. centr(i)= center, middle表示"中心;中间"
   central		[ˈsɛntr(ə)l]a.中心的,中央的;主要的
   concentrate	[ˈkɒns(ə)ntreɪt]v.集中,聚集;浓缩
   eccentric	[ɪkˈsɛntrɪk]a.古怪的,反常的
   xenocentric	[,zino'sɛntrɪk]a.偏重外国文化的
   
