   75.part=part. to divide表示"部分:分开"
   partner			n合作伙伴;搭档                          ˈpɑːtnə
   apartment		n公寓;(美)楼中的单元房,一套房间          əˈpɑːtm(ə)nt
   department		n(缩ept.)n部门(大学的)系;(百货商店)部    dɪˈpɑːtm(ə)nt
★  particular		a特别的;各自的;挑剔的                    pəˈtɪkjʊlə
★  partial			a部分的,不公平的;偏爱的,偏袒的          ˈpɑːʃ(ə)l
★  impartial		a公正的,公平的,不偏不倚的                ɪmˈpɑːʃ(ə)l
★  impartible		a不可分割的                              ɪm'pɑrtəbl
★  participate		v参加,参与                               pɑːˈtɪsɪpeɪt
★  depart			v离开,起程,出发                          dɪˈpɑːt
★  impart			v传授;告知,透露;赋予,给予                ɪmˈpɑːt
★  counterpart		n对应的人/物;副本                       ˈkaʊntəpɑːt
★  particle			n粒子,微粒,颗粒;小品词,语助词           ˈpɑːtɪk(ə)l
   
   76.grad(i)=step, degree, to walk表示"走,步;度,级"
★  grade			n等级,阶级;成绩分数;年级                ɡreɪd
★  gradual			a逐步的                                 ɡradʒʊəl
★  graduate			n毕业生;研究生,获(学士)学位者 a.研究生的 v(使)毕业;获学位 ɡradʒʊət
   centigrade		a百分度的,摄氏的,n摄氏                  ˈsɛntɪɡreɪd
★  degrade			v降级;退化,降低…的身份,有辱…的人格      dɪˈɡreɪd
★  downgrade		v贬低,降低                              ˌdaʊnˈɡreɪd
★  gradation		n等级,层次,渐变                         ɡrəˈdeɪʃ(ə)n

   105.dent= tooth表示"牙齿"
   dentist		[ˈdɛntɪst]n.牙科医生
   denture		[ˈdɛntʃə]n.假牙
   indenture	[ɪnˈdɛntʃə]n.契约,合同
   edentate		[ˈiːdənteɪt]a.无齿的n.贫齿类动物
   dent			[dɛnt]n.凹部,凹痕;缺口v在(某物)上造成凹痕
   
   106. limin,lim= threshold表示"门槛",引申为"限制"
   limit		[ˈlɪmɪt]v./n.界限,限制;极点
   eliminate	[ɪˈlɪmɪneɪt]v.消除
   preliminary	[prɪˈlɪmɪn(ə)ri]a.预备的,初步的n.[常p]初步作法,起始行为
   limp			[lɪmp]v.蹒跚n.跛行a.软弱的,无生气的;松沓的
   supraliminal	[ˌsuːprəˈlɪmɪnl]a.有意识的
   
   119.fac=face表示"脸,面"
   face			[feɪs]n.脸,面v.面向;面对
   surface		[ˈsəːfɪs]n.表面
   facet		[ˈfasɪt]n.平面;(东西的)一面;方面
   preface		[ˈprɛfəs]n.前言,序言
   deface		[dɪˈfeɪs]v.丑化;损坏外观
   
   120. memor, member,mnes= memory,to remember表示"记忆;记"
   memory		[ˈmɛm(ə)ri]n.记忆,记忆力
   memorize		[ˈmɛmərʌɪz]v.记住,熟记
   remember		[rɪˈmɛmbə]v.记得,记起;纪念
   memorable	[ˈmɛm(ə)rəb(ə)l]a.容易记住的;值得纪念的,难忘的
   memorial		[mɪˈmɔːrɪəl]n.纪念物,纪念碑a.纪念的,悼念的
   amnesiac		[amˈniːzɪak]a.记忆缺失的,(引起)遗忘(症)的

