package demo.downfile;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;  
  
  
public class DownloadImage {  
  
    /** 
     * @param args 
     * @throws Exception  
     */  
    public static void main(String[] args) throws Exception {
        String jsonFile = "D:\\temp\\word\\json\\file.txt";
        String basePath = "http://data2.readoor.cn/files/31554356777486/pdf/83756059377461/2/";
        String outPath = "D:\\temp\\word\\out";

        JSONObject jsonObj = readJsonFile(jsonFile);
        JSONArray pageListT = jsonObj.getJSONArray("page_list_t");
        JSONArray pageListS = jsonObj.getJSONArray("page_list_s");
        for (int i = 0; i < pageListS.size(); i++) {
            String urlFile = pageListS.getString(i);
            String fileName = pageListT.getString(i);
            String fileUrl = basePath + urlFile;
            download(fileUrl,fileName,outPath);
            System.out.println("下载文件="+fileName+",urlFile="+urlFile);
        }
        //download("http://data2.readoor.cn/files/31554356777486/pdf/83756059377461/2/1769b327e7bcf7652cee0305223274e9.jpg", "1.jpg","D:\\temp\\word");
    }

    public static JSONObject readJsonFile(String jsonFile) throws IOException {
        File file=new File(jsonFile);
        String content= FileUtils.readFileToString(file,"UTF-8");
        JSONObject jsonObj = (JSONObject) JSON.parse(content);
        return jsonObj;
    }
      
    public static void download(String urlString, String filename,String savePath) throws Exception {  
        // 构造URL  
        URL url = new URL(urlString);  
        // 打开连接  
        URLConnection con = url.openConnection();  
        //设置请求超时为5s  
        con.setConnectTimeout(5*1000);  
        // 输入流  
        InputStream is = con.getInputStream();  
      
        // 1K的数据缓冲  
        byte[] bs = new byte[1024];  
        // 读取到的数据长度  
        int len;  
        // 输出的文件流  
       File sf=new File(savePath);  
       if(!sf.exists()){  
           sf.mkdirs();  
       }  
       OutputStream os = new FileOutputStream(sf.getPath()+"\\"+filename);  
        // 开始读取  
        while ((len = is.read(bs)) != -1) {  
          os.write(bs, 0, len);  
        }  
        // 完毕，关闭所有链接  
        os.close();  
        is.close();  
    }   
  
}  