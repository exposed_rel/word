   91.simil, simul(t),sembl= alike,same表示“相类似,一样
   similar			a同样的;相似的,像                ˈsɪmɪlə
   similarity		n类似,相似点                      sɪməˈlarəti
   dissimilar		a不同的                          dɪˈsɪmɪlə
   verisimilar		a像真的一样,貌似真实的;可能的    ˌvɛrɪˈsɪmɪlə
   simulate			v假装,模仿,模拟;冒充             ˈsɪmjʊleɪt ,ate使一样->模仿
   simultaneous		a同时发生的,同时存在的,同步的    ˌsɪm(ə)lˈteɪnɪəs ,aneous...特征->相识的特征
   resemble			v类似,像                         rɪˈzɛmb(ə)l
   resemblance		n类似,相似,形似                  rɪˈzɛm bl(ə)ns
   assemble			v集合,召集,装配                  əˈsɛmb(ə)l
   
   92.vi(a),vey,voy=way表示“道路”
   via				[介词prep]经过      ˈvʌɪə
   viaduct			n高架桥             ˈvʌɪədʌkt
   deviate			v越轨,背离          ˈdiːvɪeɪt
   obvious			a明显的             ˈɒbvɪəs
   previous			a以前的,早先的      ˈpriːvɪəs
   trivial			a琐碎的;不重要的    ˈtrɪvɪəl
   convey			v运输,传达;搬运     kənˈveɪ
   convoy			v护送,护航          ˈkɒnvɔɪ

