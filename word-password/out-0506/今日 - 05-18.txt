   79. order. ordin= order表示"命令,秩序"
   order			n次序;命令;v命令,定购,点菜             ˈɔːdə
★  disorder			v混乱,骚乱,失调                         dɪsˈɔːdə
★  ordinary			a普通的,平常的                         ˈɔːdɪn(ə)ri
★  extraordinary	a特别的,非常的                          ɪkˈstrɔːd(ə)n(ə)ri
★  ordinal			a顺序的                                 ˈɔːdɪn(ə)l
★  coordinate		v协作,协调;a同等的,坐标的;n坐标         kəʊˈɔːdɪneɪt
★  coordinator		n协调者                                 ˌkəʊˈɔːdɪneɪtə
★  subordinate		a附属的,从属的;n下属;v使处于次要地位     səˈbɔːdɪnət
★  insubordinate	a不服从的;反抗的,犯上的                 ˌɪnsəˈbɔːdɪnət

   80. press= to press, to force表示"挤压;逼迫"
★  pressure			n压力                               ˈprɛʃə
★  depress			v压下,按下;使沮丧,使不景气          dɪˈprɛs
★  depression		n沮丧,萧条,洼地,凹陷                dɪˈprɛʃ(ə)n
★  impress			v盖印;给人印象深刻                  ɪmˈprɛs
★  impression		n印象;感想                          ɪmˈprɛʃ(ə)n
★  impressive		a给人深刻印象的                     ɪmˈprɛsɪv
★  suppress			v镇压,压制,抑制,阻止                səˈprɛs
★  suppression		n镇压                               səˈprɛʃ(ə)n
★  express			v表达,表示;挤压出;快递;n快车;快递   ɪkˈsprɛs
★  expression		n表达;表情                          ɪkˈsprɛʃ(ə)n
   compress			v压缩,压紧                          kəmˈprɛs
   compression		n压缩                               kəmˈprɛʃ(ə)n
★  repress			v镇压;抑制(感情)                    rɪˈprɛs
★  pressing			a急迫的                             ˈprɛsɪŋ

   109.vail,val(u)= value, worth表示"价值,值"
   available	[əˈveɪləb(ə)l]a.有效的;有空的;可得到的
   valuable		[ˈvaljʊb(ə)l]a.有价值的;贵重的
   evaluate		[ɪˈvaljʊeɪt]v.估价
   equivalent	[ɪˈkwɪv(ə)l(ə)nt]a.相等的n.等价物
   invaluable	[ɪnˈvaljʊ(ə)b(ə)l]a.非常宝贵的,无价的
   undervalue	[ʌndəˈvaljuː]v.低估;轻视
   
   110.jud,judg, judic(i)= to judge,law表示"判断;法律"
   prejudice	[ˈprɛdʒʊdɪs]n.偏见
   adjudge		[əˈdʒʌdʒ]判决,裁决
   judgment		[ˈdʒʌdʒmənt]n.判断,判断力;审判;意见
   prejudge		[priːˈdʒʌdʒ]v.预先断定
   judicial		[dʒuːˈdɪʃ(ə)l]a.司法的;法官的
   adjudicate	[əˈdʒuːdɪkeɪt]v.判决,裁判
   
