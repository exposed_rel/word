   117. numer= number, to count表示"数;计数"
   numerable	[ˈnjuːm(ə)rəb(ə)l]a.可数的,可计算的
   numerous		[ˈnjuːm(ə)rəs]a.无数的,众多的
   innumerable	[ɪˈnjuːm(ə)rəb(ə)l]a.无数的,数不清的
   supernumerary[ˌsuːpəˈnjuːm(ə)r(ə)ri]   supernumerary	[sju: panju: maram.额外的
   enumerate	[ɪˈnjuːməreɪt]v.列举
   
   118. centr(i)= center, middle表示"中心;中间"
   central		[ˈsɛntr(ə)l]a.中心的,中央的;主要的
   concentrate	[ˈkɒns(ə)ntreɪt]v.集中,聚集;浓缩
   eccentric	[ɪkˈsɛntrɪk]a.古怪的,反常的
   xenocentric	[,zino'sɛntrɪk]a.偏重外国文化的
   
